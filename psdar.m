function [F,Pxx,LF,HF,VLF,LFnu,HFnu,LF_HF,TotalPower,iRR,Time,Fs,window_ar,order,bands] =...
    psdar(iRR,Time,Fs,ax)

if Time(3) - Time(2) ~= Time(2) - Time(1)
    er = errordlg('Data are not Even Spaced. Please Re-sample','Error','modal');
    uiwait(er)
    [Time,iRR,Fs] = preprocessing(iRR,Time);
end
F=NaN;
Pxx=NaN;
LF=NaN;
HF=NaN;
VLF=NaN;
LFnu=NaN;
HFnu=NaN;
LF_HF=NaN;
TotalPower=NaN;
window_ar=NaN;
order=NaN;
bands=NaN;

if isempty(Fs);
    prompt = {'Sampling Frequency','Enter the Order:'};
    dlg_title = 'Auto Regressive Power Spectral Density Parameters';
    num_lines = 1;
    def = {'4','14'};
    answer = inputdlg(prompt,dlg_title,num_lines,def);
    if isempty(cellfun(@isempty,answer))
        return
    else
        Fs = str2double(answer{1});
        order = str2double(answer{2});
    end
else
    prompt = {'Enter the Order:'};
    dlg_title = 'Auto Regressive Power Spectral Density Parameters';
    num_lines = 1;
    def = {'14'};
    answer = inputdlg(prompt,dlg_title,num_lines,def);
    if isempty(cellfun(@isempty,answer))
        return
    else
        order = str2double(answer{1});
    end
end
[window, control] = window_select();

if control ~= 0,
    switch window
        case 1
            iRR = iRR .*hanning(length(iRR))';
            [Pxx,F] = pburg(iRR,order,[],Fs);
            window_w = 'Hanning';
        case 3
            iRR = iRR .*blackman(length(iRR))';
            [Pxx,F] = pburg(iRR,order,[],Fs);
            window_w = 'Blackman';
        case 4
            iRR = iRR .*hamming(length(iRR))';
            [Pxx,F] = pburg(iRR,order,[],Fs);
            window_w = 'Hamming';
        case 2
            iRR = iRR .*triang(length(iRR))';
            [Pxx,F] = pburg(iRR,order,[],Fs);
            window_w = 'Triangular';
        case 5
            iRR = iRR .*kaiser(length(iRR))';
            [Pxx,F] = pburg(iRR,order,[],Fs);
            window_w = 'Kaiser';
        case 6
            iRR = iRR .*gausswin(length(iRR))';
            [Pxx,F] = pburg(iRR,order,[],Fs);
            window_w = 'Gaussian';
    end
else
    [Pxx,F] = pburg(iRR,order,[],Fs);
    window_w = 'Rectangular';
end

[LF,HF,VLF,LFnu,HFnu,LF_HF,TotalPower,bands] = psdintegral(F,Pxx);
axes(ax)
area(F,Pxx)
hold on
H1=area(F(find(F>= bands(1)):find(F>=bands(2))),abs(Pxx(find(F>= bands(1)):find(F>=bands(2)))));
set(H1(1),'FaceColor',[173/256 255/256 47/256]);
H2=area(F(find(F>= bands(3)):find(F>=bands(4))),abs(Pxx(find(F>= bands(3)):find(F>=bands(4)))));
set(H2(1),'FaceColor',[1 1 1]);
H3=area(F(find(F>= bands(5)):find(F>=bands(6))),abs(Pxx(find(F>= bands(5)):find(F>=bands(6)))));
set(H3(1),'FaceColor',[100/256 149/256 237/256]);
hold off
title('Power Spectral Density Estimative via Auto Regressive Method')
xlabel('Frequency (Hz)')
ylabel('PSD (ms^2/Hz)')
end