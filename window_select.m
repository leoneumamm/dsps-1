function [window, control] = window_select()
options = {'Hanning','Triangular','Blackman','Hamming','Kaiser','Gaussian'};
[window, control] = listdlg('PromptString','Select a window:',...
    'SelectionMode','single',...
    'ListString',options);
end