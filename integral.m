function [alta,baixa] = integral(Pft,frequencia,iter)

askband = figure('Position',[300,300,200,200],'Menubar','None','resize','off',...
    'color',[0.839,0.91,0.851],'WindowStyle','Modal');
tvlf = uicontrol('Style','Text','String','VLF Band','Position',[70, 178,50,20]...
    ,'backgroundcolor',[0.839,0.91,0.851]);
vlf1 = uicontrol('Style','Edit','String','0.003','Position',[20,150,70,20]);
vlf2 = uicontrol('Style','Edit','String','0.04','Position',[95,150,70,20]);
disp(vlf1)
tlf = uicontrol('Style','Text','String','LF Band','Position',[70, 128,50,20],...
    'backgroundcolor',[0.839,0.91,0.851]);
lf1 = uicontrol('Style','Edit','String','0.04','Position',[20,100,70,20]);
lf2 = uicontrol('Style','Edit','String','0.15','Position',[95,100,70,20]);
thf = uicontrol('Style','Text','String','HF Band','Position',[70, 78,50,20],...
    'backgroundcolor',[0.839,0.91,0.851]);
hf1 = uicontrol('Style','Edit','String','0.15','Position',[20,50,70,20]);
hf2= uicontrol('Style','Edit','String','0.5','Position',[95,50,70,20]);
btband = uicontrol('String','Ok','CallBack','uiresume(gcbf)');
uiwait(askband)
close(askband)
HF_ind = find(frequencia >= hf1 & frequencia < hf2);

LF_ind = find(frequencia >= lf1 & frequencia < lf2);

inc = frequencia(2)-frequencia(1);
for g = 1:iter,
    baixa(:,g) = trapz(Pft(LF_ind,g)).*(inc);
    alta(:,g) = trapz(Pft(HF_ind,g)).*(inc);
end
end


% Escala em dB.Hz!